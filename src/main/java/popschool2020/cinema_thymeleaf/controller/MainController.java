package popschool2020.cinema_thymeleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool2020.cinema_thymeleaf.form.ClientForm;
import popschool2020.cinema_thymeleaf.model.Client;
import popschool2020.cinema_thymeleaf.service.VideoService;
import popschool2020.cinema_thymeleaf.utils.WebUtils;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;


@Controller
public class MainController {
    // Sert à communiquer entre la partie DAO en java et la partie front en HTML

    private static List<Client> clients = new ArrayList<Client>();

    //Injection du service Video DAO
    @Autowired
    VideoService videoDAO;


    // Injectez (inject) via application.properties.
    @Value("${welcome.message}")
    private String message;
    @Value("${error.message}")
    private String errorMessage;

    @RequestMapping(value = { "/welcome" }, method = RequestMethod.GET)
    public String welcomePage(Model model, Principal principal) {
        User logedInUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(logedInUser);
        model.addAttribute("userInfo", userInfo);
        model.addAttribute("title", "Welcome");
        model.addAttribute("message", "This is welcome page!");
        return "welcomePage";
    }

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("message", message);
        return "loginPage";
    }

    @RequestMapping(value = {"/actorList"}, method = RequestMethod.GET)
    public String actorList(Model model) {
        model.addAttribute("actors", videoDAO.ensActeur());
        return "actorList";
    }

    @RequestMapping(value = {"/genreList"}, method = RequestMethod.GET)
    public String selectionGenre(Model model) {
        model.addAttribute("listeGenre", videoDAO.ensGenres());
        return "genreList";
    }

    @PostMapping("/voirFilm")
    public String films(@RequestParam("genreChoisi")String nature, Model model) {
        model.addAttribute("ensFilmChoisi", videoDAO.findByGenre(nature));
        return "filmList";
    }

    @RequestMapping(value = {"/filmListByActor"}, method = RequestMethod.GET)
    public String actorChooseList(Model model) {
        model.addAttribute("actorsList", videoDAO.ensActeur());
        return "actorChooseList";
    }

    @PostMapping("/voirFilmByActor")
    public String filmsByActor(@RequestParam("actorChoisi")String nom, Model model) {
        model.addAttribute("ensFilmChoisiByActor", videoDAO.ensFilmsByActor(nom));
        return "filmListByActor";
    }

    @RequestMapping(value = {"/selectClientAndFilm"}, method = RequestMethod.GET)
    public String listClients(Model model) {
        model.addAttribute("clientList", videoDAO.ensClients());
        model.addAttribute("filmDispoList", videoDAO.ensFilmsEmpruntables());
        return "selectClientAndFilm";
    }

    @PostMapping("/emprunterFilm")
    public String emprunterFilm(@RequestParam("clientChoisi")String nom, @RequestParam("filmChoisi")String titre, Model model) {
        videoDAO.emprunter(nom, titre);
        return empruntList(model);
    }

    @RequestMapping(value = {"/selectEmprunt"}, method = RequestMethod.GET)
    public String listEmprunt(Model model) {
        model.addAttribute("empruntSelectList", videoDAO.ensFilmsNonRendu());
        return "selectFilmRestituer";
    }

    @PostMapping("/restituerFilm")
    public String restituerFilm(@RequestParam("empruntChoisi")Long id, Model model) {
        videoDAO.retourEmpruntId(id);
        return empruntList(model);
    }

    private String empruntList(Model model) {
        model.addAttribute("empruntList", videoDAO.ensEmprunt());
        return "empruntList";
    }

    @RequestMapping(value = {"/addClient"}, method = RequestMethod.GET)
    public String showAddClientPage(Model model) {
        ClientForm clientForm = new ClientForm();
        model.addAttribute("clientForm", clientForm);
        return "addClient";
    }

    //Création d'un client via la classe form.ClientForm
    @RequestMapping(value = {"/addClient"}, method = RequestMethod.POST)
    public String saveEmployee(Model model, //
                               @ModelAttribute("clientForm") ClientForm clientForm) {

        String nom = clientForm.getNom();
        String prenom = clientForm.getPrenom();
        String adresse = clientForm.getAdresse();
        BigDecimal anciennete = clientForm.getAnciennete();

        if (nom != null && nom.length() > 0
                && prenom != null && prenom.length() > 0) {
            Client newClient = new Client(nom, prenom, adresse, anciennete);
            clients.add(newClient);
            videoDAO.addClient(newClient);
            return clientList(model);
        }
        model.addAttribute("errorMessage", errorMessage);
        return "addClient";
    }

    //Suppression d'un client via le nom passé en formulaire HTML
    @GetMapping("/delete/{nom}")
    public String deleteClient(@PathVariable("nom") String nom, Model model) {
        videoDAO.deleteClient(nom);
        return clientList(model);
    }

    @RequestMapping(value = {"/clientList"}, method = RequestMethod.GET)
    public String clientList(Model model) {
        model.addAttribute("clients", videoDAO.ensClients());
        return "clientList";
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accessDenied(Model model, Principal principal) {

        if (principal != null) {
            User loginedUser = (User) ((Authentication) principal).getPrincipal();

            String userInfo = WebUtils.toString(loginedUser);

            model.addAttribute("userInfo", userInfo);

            String message = "Hi " + principal.getName() //
                    + "<br> You do not have permission to access this page!";
            model.addAttribute("message", message);

        }

        return "403Page";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(Model model) {
        return "loginPage";
    }

    @RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
    public String logoutSuccessfulPage(Model model) {
        model.addAttribute("title", "Logout");
        return "logoutSuccessfulPage";
    }

}
