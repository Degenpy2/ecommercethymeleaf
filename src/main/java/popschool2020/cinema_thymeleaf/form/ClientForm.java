package popschool2020.cinema_thymeleaf.form;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ClientForm {
    private Long nclient;
    private String nom;
    private String prenom;
    private String adresse;
    private BigDecimal anciennete;

    public ClientForm(String nom, String prenom, String adresse, BigDecimal anciennete) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.anciennete = anciennete;
    }

    public ClientForm() {
    }

    public Long getNclient() {
        return nclient;
    }

    public void setNclient(Long nclient) {
        this.nclient = nclient;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public BigDecimal getAnciennete() {
        return anciennete;
    }

    public void setAnciennete(BigDecimal anciennete) {
        this.anciennete = anciennete;
    }


}
