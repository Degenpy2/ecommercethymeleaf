package popschool2020.cinema_thymeleaf.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="CLIENT")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long nclient;
    private String nom;
    private String prenom;
    private String adresse;
    private BigDecimal anciennete;

    public Client() {
    }

    public Client(String nom, String prenom, String adresse, BigDecimal anciennete) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.anciennete = anciennete;
    }


    public Long getNclient() {
        return this.nclient;
    }

    public String getNom() {
        return this.nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public BigDecimal getAnciennete() {
        return this.anciennete;
    }

    public void setNclient(Long nclient) {
        this.nclient = nclient;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setAnciennete(BigDecimal anciennete) {
        this.anciennete = anciennete;
    }

    public String toString() {
        return "Client(nclient=" + this.getNclient() + ", nom=" + this.getNom() + ", prenom=" + this.getPrenom() + ", adresse=" + this.getAdresse() + ", anciennete=" + this.getAnciennete() + ")";
    }
}
