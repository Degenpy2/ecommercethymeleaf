package popschool2020.cinema_thymeleaf.model;

import javax.persistence.*;

@Entity
@Table(name = "APP_ROLE", //
        uniqueConstraints = { //
                @UniqueConstraint(name = "APP_ROLE_UK", columnNames = "ROLE_NAME") })
public class AppRole {

    @Id
    @GeneratedValue
    @Column(name = "ROLE_ID", nullable = false)
    private Long id;

    @Column(name = "ROLE_NAME", length = 30, nullable = false)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
