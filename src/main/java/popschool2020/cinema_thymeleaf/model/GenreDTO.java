package popschool2020.cinema_thymeleaf.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


public class GenreDTO {
    private String nature;
    private Long nb;

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Long getNb() {
        return nb;
    }

    public void setNb(Long nb) {
        this.nb = nb;
    }

    public GenreDTO(String nature, Long nb) {
        this.nature = nature;
        this.nb = nb;
    }

    public GenreDTO() {
    }

    @Override
    public String toString() {
        return "GenreDTO{" +
                "nature='" + nature + '\'' +
                ", nb=" + nb +
                '}';
    }
}
