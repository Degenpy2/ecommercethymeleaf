package popschool2020.cinema_thymeleaf.model;

import javax.persistence.*;

@Entity
@Table(name = "APP_USER", //
        uniqueConstraints = { //
                @UniqueConstraint(name = "APP_USER_UK", columnNames = "USER_NAME") })
public class AppUser {

    @Id
    @GeneratedValue
    @Column(name = "USER_ID", nullable = false)
    private Long id;

    @Column(name = "USER_NAME", length = 36, nullable = false)
    private String name;

    @Column(name = "ENCRYTED_PASSWORD", length = 128, nullable = false)
    private String encrytedPassword;

    @Column(name = "ENABLED", length = 1, nullable = false)
    private boolean enabled;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEncrytedPassword() {
        return encrytedPassword;
    }

    public void setEncrytedPassword(String encrytedPassword) {
        this.encrytedPassword = encrytedPassword;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
