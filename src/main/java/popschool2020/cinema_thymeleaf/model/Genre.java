package popschool2020.cinema_thymeleaf.model;

import javax.persistence.*;

@Entity
@Table(name="GENRE")
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ngenre;
    private String nature;

    public Long getNgenre() {
        return ngenre;
    }

    public void setNgenre(Long ngenre) {
        this.ngenre = ngenre;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Genre() {
    }

    @Override
    public String toString() {
        return "Genre{" +
                "ngenre=" + ngenre +
                ", nature='" + nature + '\'' +
                '}';
    }
}
