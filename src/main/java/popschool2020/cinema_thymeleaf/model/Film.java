package popschool2020.cinema_thymeleaf.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
@Entity
@Table(name="FILM")
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long nfilm;
    private String titre;
    @ManyToOne
    @JoinColumn(name = "ngenre")
    private Genre genre;
    private Date sortie;
    @ManyToOne
    @JoinColumn(name = "npays")
    private Pays pays;
    private String realisateur;
    @ManyToOne
    @JoinColumn(name = "nacteurprincipal")
    private Acteur acteur;
    private BigDecimal entrees;
    private String oscar;

    public Film() {
    }

    public Film(String titre, Genre genre, Date sortie, Pays pays, String realisateur, Acteur acteur, BigDecimal entrees, String oscar) {
    this.titre = titre;
    this.genre = genre;
    this.sortie = sortie;
    this.pays = pays;
    this.realisateur = realisateur;
    this.acteur = acteur;
    this.entrees = entrees;
    this.oscar = oscar;
    }

    public Long getNfilm() {
        return this.nfilm;
    }

    public String getTitre() {
        return this.titre;
    }


    public Date getSortie() {
        return this.sortie;
    }

    public Pays getPays() {
        return this.pays;
    }

    public String getRealisateur() {
        return this.realisateur;
    }

    public Acteur getActeur() {
        return this.acteur;
    }

    public BigDecimal getEntrees() {
        return this.entrees;
    }

    public String getOscar() {
        return this.oscar;
    }

    public void setNfilm(Long nfilm) {
        this.nfilm = nfilm;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }



    public void setSortie(Date sortie) {
        this.sortie = sortie;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public void setActeur(Acteur acteur) {
        this.acteur = acteur;
    }

    public void setEntrees(BigDecimal entrees) {
        this.entrees = entrees;
    }

    public void setOscar(String oscar) {
        this.oscar = oscar;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String toString() {
        return "Film(nfilm=" + this.getNfilm() + ", titre=" + this.getTitre() + ", ngenre=" + this.getGenre() + ", sortie=" + this.getSortie() + ", pays=" + this.getPays() + ", realisateur=" + this.getRealisateur() + ", acteur=" + this.getActeur() + ", entrees=" + this.getEntrees() + ", oscar=" + this.getOscar() + ")";
    }
}
