package popschool2020.cinema_thymeleaf.model;

import javax.persistence.*;

@Entity
@Table(name="PAYS")
public class Pays {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long npays;
    private String nom;

    public Pays() {
    }

    public Pays(String nom) {
        this.nom = nom;
    }

    public Long getNpays() {
        return npays;
    }

    public void setNpays(Long npays) {
        this.npays = npays;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Pays{" +
                "npays=" + npays +
                ", nom='" + nom + '\'' +
                '}';
    }
}
