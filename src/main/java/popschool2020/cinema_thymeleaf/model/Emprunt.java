package popschool2020.cinema_thymeleaf.model;

import javax.persistence.*;
import java.sql.Date;
@Entity
@Table(name="EMPRUNT")
public class Emprunt {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long nemprunt;
    @ManyToOne
    @JoinColumn(name = "nclient")
    private Client client;
    @ManyToOne
    @JoinColumn(name = "nfilm")
    private Film film;
    private String retour;
    private Date dateEmprunt;

    public Emprunt() {
    }

    public Emprunt(Client client, Film film, String retour, Date dateEmprunt) {
        this.client = client;
        this.film = film;
        this.retour = retour;
        this.dateEmprunt = dateEmprunt;
    }


    public Long getNemprunt() {
        return this.nemprunt;
    }

    public Client getClient() {
        return this.client;
    }

    public Film getFilm() {
        return this.film;
    }

    public String getRetour() {
        return this.retour;
    }

    public Date getDateEmprunt() {
        return this.dateEmprunt;
    }

    public void setNemprunt(Long nemprunt) {
        this.nemprunt = nemprunt;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public void setRetour(String retour) {
        this.retour = retour;
    }

    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }

    public String toString() {
        return "Emprunt(nemprunt=" + this.getNemprunt() + ", client=" + this.getClient() + ", film=" + this.getFilm() + ", retour=" + this.getRetour() + ", dateEmprunt=" + this.getDateEmprunt() + ")";
    }
}
