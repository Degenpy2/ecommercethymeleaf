package popschool2020.cinema_thymeleaf.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="ACTEUR")
public class Acteur {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long nacteur;
    private String nom;
    private String prenom;
    private Date naissance;
    @ManyToOne
    @JoinColumn(name = "nationalite")
    private Pays pays;
    private Integer nbrefilms;

    public Long getNacteur() {
        return nacteur;
    }

    public void setNacteur(Long nacteur) {
        this.nacteur = nacteur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getNaissance() {
        return naissance;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }

    public Pays getNationalite() {
        return pays;
    }

    public void setNationalite(Pays pays) {
        this.pays = pays;
    }

    public Integer getNbrefilms() {
        return nbrefilms;
    }

    public void setNbrefilms(Integer nbrefilms) {
        this.nbrefilms = nbrefilms;
    }

    public Acteur(String nom, String prenom, Date naissance, Pays pays, Integer nbrefilms) {
    this.nom = nom;
    this.prenom = prenom;
    this.naissance = naissance;
    this.pays = pays;
    this.nbrefilms = nbrefilms;
    }

    public Acteur() {
    }

    @Override
    public String toString() {
        return "Acteur{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", naissance=" + naissance +
                ", pays=" + (pays!=null?", " + pays.getNom() : "") +
                ", nbrefilms=" + nbrefilms +
                '}';
    }
}
