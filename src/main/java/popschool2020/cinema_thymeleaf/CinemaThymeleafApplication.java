package popschool2020.cinema_thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemaThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(CinemaThymeleafApplication.class, args);
    }

}
