package popschool2020.cinema_thymeleaf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import popschool2020.cinema_thymeleaf.dao.*;
import popschool2020.cinema_thymeleaf.model.*;

import javax.persistence.Tuple;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class VideoServiceImpl implements VideoService {

    @Autowired
    ClientRepository clientDAO;

    @Autowired
    FilmRepository filmDAO;

    @Autowired
    EmpruntRepository empruntDAO;

    @Autowired
    GenreRepository genreDAO;

    @Autowired
    ActeurRepository acteurDAO;

    public void emprunter(String nom, String titre) {
        Optional<Client> opCl = clientDAO.findByNom(nom);
        Optional<Film> opFilm = filmDAO.findByTitre(titre);
        if(opCl.isPresent() && opFilm.isPresent()){
            java.util.Date utilDate=new java.util.Date();
            empruntDAO.save(new Emprunt(opCl.get(),opFilm.get(),"NON",new Date(utilDate.getTime())));
        }

    }

    public List<Client> ensClients() {
        return clientDAO.findAll();
    }

    public List<Film> ensFilms() {
        return filmDAO.findAll();
    }

    @Override
    public List<Film> ensFilmsByActor(String nom) {
        return filmDAO.findByActeur_Nom(nom);
    }

    @Override
    public List<Emprunt> ensEmprunt() {
        return empruntDAO.findAll();
    }

    @Override
    public List<Genre> ensGenres() {
        return genreDAO.findAll();
    }

    @Override
    public List<Acteur> ensActeur() {
        return acteurDAO.findAll();
    }

    @Override
    public List<Film> ensFilmDuGenre(String nature) {
        return filmDAO.findByGenre_Nature(nature);
    }

    @Override
    public List<Tuple> infoRealisateurActeur(String unTitre) {
        return filmDAO.findRealisateurAndActeurByTitre(unTitre);
    }

    @Override
    public int nombreFilmDuGenre(String nature) {
        return filmDAO.countFilmByGenre_Nature(nature);
    }

    @Override
    public List<Film> ensFilmsEmpruntables() {
        return empruntDAO.findFilmEmpruntable();
    }

    @Override
    public List<Film> ensFilmsEmpruntes(Client client) {
        return empruntDAO.findFilmEmprunte(client);
    }

    @Override
    public List<Emprunt> ensFilmsNonRendu() {
        return empruntDAO.findEmpruntNonRendu();
    }

    @Override
    public void retourEmprunt(String nomClient, String titreFilm) {
        Client client = new Client();
        Film film = new Film();
        Emprunt emprunt = new Emprunt();
        Optional<Client> optionalClient = clientDAO.findByNom(nomClient);
        if(optionalClient.isPresent()){
            client = optionalClient.get();
        }
        Optional<Film> optionalFilm = filmDAO.findByTitre(titreFilm);
        if(optionalFilm.isPresent()){
            film = optionalFilm.get();
        }
        if(client.getNom() != null && film.getTitre() != null){
            Optional<Emprunt> optionalEmprunt = empruntDAO.findByClientAndFilm(client, film);
            if(optionalEmprunt.isPresent()){
                emprunt = optionalEmprunt.get();
            }
        }
        emprunt.setRetour("OUI");
        empruntDAO.save(emprunt);
    }

    @Override
    public void retourEmpruntId(Long id) {
        Optional<Emprunt> optionalEmprunt1 = empruntDAO.findById(id);
        if(optionalEmprunt1.isPresent()){
            Emprunt emprunt1 = optionalEmprunt1.get();
            emprunt1.setRetour("OUI");
            empruntDAO.save(emprunt1);
        }
    }

    @Override
    public void addClient(Client client) {
        clientDAO.save(client);
    }

    @Override
    public List<Film> findByGenre(String genre) {
        return filmDAO.findByGenre_Nature(genre);
    }

    //Suppression d'un client et de tous les emprunts qui lui sont associés
    public void deleteClient(String nom) {
        Optional<Client> optionalClient = clientDAO.findByNom(nom);
        if(optionalClient.isPresent()){
            empruntDAO.supprimerEmpruntsClient(optionalClient.get());
            clientDAO.deleteByNom(nom);
        }


    }
}
