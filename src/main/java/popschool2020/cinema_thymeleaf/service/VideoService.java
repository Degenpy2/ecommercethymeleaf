package popschool2020.cinema_thymeleaf.service;


import popschool2020.cinema_thymeleaf.model.*;

import javax.persistence.Tuple;
import java.util.List;

public interface VideoService {
    void emprunter(String nom, String titre);
    void deleteClient(String nom);
    List<Client> ensClients();
    List<Film> ensFilms();

    List<Emprunt> ensEmprunt();

    List<Genre> ensGenres();
    List<Acteur> ensActeur();
    List<Film> ensFilmDuGenre(String nature);
    List<Tuple> infoRealisateurActeur(String unTitre);
    int nombreFilmDuGenre(String nature);
    List<Film> ensFilmsEmpruntables();
    List<Film> ensFilmsByActor(String nom);
    List<Film> ensFilmsEmpruntes(Client client);
    List<Emprunt> ensFilmsNonRendu();
    void retourEmprunt(String nomClient, String titreFilm);

    void retourEmpruntId(Long id);

    void addClient(Client client);

    List<Film> findByGenre(String genre);
}
