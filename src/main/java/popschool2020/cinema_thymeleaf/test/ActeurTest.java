package popschool2020.cinema_thymeleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool2020.cinema_thymeleaf.CinemaThymeleafApplication;
import popschool2020.cinema_thymeleaf.dao.ActeurRepository;
import popschool2020.cinema_thymeleaf.model.Acteur;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Component
public class ActeurTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(CinemaThymeleafApplication.class);

    @Autowired
    private ActeurRepository repository;

    private void information(Acteur a){
        log.info(a.toString());
    }


    @Override
    public void run(String... args) throws Exception {
        /*// fetch all customer

        log.info("Acteurs found with findAll();");
        log.info("----------------------");
        for(Acteur acteur : repository.findAll()) {
            log.info(acteur.toString());
        }
        log.info("");

        // fetch an individual customer by ID
        Optional<Acteur> acteur = repository.findById(1L);
        log.info("Act found with findById(1L)");
        log.info("----------------------");
        if(acteur.isPresent()) {
            Acteur act = acteur.get();
            log.info(acteur.get().toString());
            log.info("");
            repository.findAll().forEach(this::information);
            log.info("");
        }

        log.info("Acteur found with findByNom();");
        log.info("----------------------");
        List<Acteur> acteurs = repository.findByNom("Allen");
        if(acteurs.size()>0){
            log.info(acteurs.get(0).toString());
        }
        log.info("");

        log.info("Acteur found with findByPrenom();");
        log.info("----------------------");
        repository.findByPrenom("Jack").forEach(actor -> {
            log.info(actor.toString());
        });
        log.info("");

        log.info("Acteur found with findByNaissanceAfter();");
        log.info("----------------------");
        Date born = Date.valueOf("1960-01-01");
        repository.findByNaissanceAfter(born).forEach(actor -> {
            log.info(actor.toString());
        });
        log.info("");

        log.info("Acteur found with findByNaissanceBefore();");
        log.info("----------------------");
        Date born1 = Date.valueOf("1960-01-01");
        repository.findByNaissanceBefore(born1).forEach(actor -> {
            log.info(actor.toString());
        });
        log.info("");

        log.info("Acteur found with findByNbrefilmsGreaterThan();");
        log.info("----------------------");
        repository.findByNbrefilmsGreaterThan(3).forEach(actor -> {
            log.info(actor.toString());
        });
        log.info("");

        log.info("Acteur found with findByNomContaining;");
        log.info("----------------------");
        repository.findByNomContaining("a").forEach(actor -> {
            log.info(actor.toString());
        });
        log.info("");


        log.info("Acteur found with findByPays_nom;");
        log.info("----------------------");
        repository.findByPays_nom("Irlande").forEach(actor -> {
            log.info(actor.toString());
        });
        log.info("");

        log.info("Acteur found with findByPays_npays;");
        log.info("----------------------");
        repository.findByPays_npays(1L).forEach(actor -> {
            log.info(actor.toString());
        });
        log.info("");

        log.info("Acteur found with findFilmsByGenre;");
        log.info("----------------------");
        repository.findActeurByPays().forEach(pays -> {
            log.info(pays.get("pays", String.class)+
                    " " +
                    pays.get("nombre", Long.class)
            );
        });
        log.info("");*/

    }
}
