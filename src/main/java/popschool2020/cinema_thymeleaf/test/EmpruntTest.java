package popschool2020.cinema_thymeleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool2020.cinema_thymeleaf.CinemaThymeleafApplication;
import popschool2020.cinema_thymeleaf.dao.ClientRepository;
import popschool2020.cinema_thymeleaf.dao.EmpruntRepository;
import popschool2020.cinema_thymeleaf.dao.FilmRepository;
import popschool2020.cinema_thymeleaf.model.Client;
import popschool2020.cinema_thymeleaf.model.Emprunt;
import popschool2020.cinema_thymeleaf.model.Film;
import popschool2020.cinema_thymeleaf.service.VideoServiceImpl;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Optional;

@Component
public class EmpruntTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(CinemaThymeleafApplication.class);

    @Autowired
    private EmpruntRepository repository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private VideoServiceImpl videoRepository;


    private void information(Emprunt e) {
        log.info(e.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        /*log.info("Emprunt found with findByClient_Nclient;");
        log.info("----------------------");
        for(Emprunt emprunt : repository.findByClient_Nclient(1L)) {
            log.info(emprunt.toString());
        }
        log.info("");

        for(Emprunt emprunt : repository.findAll()) {
            log.info(emprunt.toString());
        }
        repository.supprimerEmpruntsClientId(1L);
        for(Emprunt emprunt : repository.findAll()) {
            log.info(emprunt.toString());
        }

        log.info("Emprunt found with findAll();");
        log.info("----------------------");
        for(Emprunt emprunt : repository.findAll()) {
            log.info(emprunt.toString());
        }
        log.info("");

        Optional<Emprunt> emprunt = repository.findById(1L);
        log.info("emprunt found with findById(1L)");
        log.info("----------------------");
        if(emprunt.isPresent()) {
            Emprunt emp = emprunt.get();
            log.info(emprunt.get().toString());
            log.info("");
            repository.delete(emp);
            repository.findAll().forEach(this::information);
            log.info("");
        Optional<Film> optionalFilm = filmRepository.findById(2L);
        Film film=new Film();
        if(optionalFilm.isPresent()){
            film = optionalFilm.get();
        }

        Optional<Client> optionalClient = clientRepository.findById(6L);
        Client client=new Client();
        if(optionalClient.isPresent()){
            client = optionalClient.get();
        }
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        Emprunt emprunt = new Emprunt();
        emprunt.setClient(client);
        emprunt.setFilm(film);
        emprunt.setRetour("NON");
        emprunt.setDateEmprunt(date);

        repository.save(emprunt);

        log.info("Emprunt found with findFilmEmpruntable();");
        log.info("----------------------");
        for (Film fil : repository.findFilmEmpruntable()) {
            log.info(fil.toString());
        }
        log.info("");


        Optional<Client> optionalClient = clientRepository.findById(6L);
        Client client=new Client();
        if(optionalClient.isPresent()){
            client = optionalClient.get();
        }

        log.info("Emprunt found with findFilmEmprunte();");
        log.info("----------------------");
        for (Film fil : repository.findFilmEmprunte(client)) {
            log.info(fil.toString());
        }
        log.info("");

        //videoRepository.retourEmprunt("PATTULACCI", "West side story");

        log.info("Acteur found with findFilmsByGenre;");
        log.info("----------------------");
        videoRepository.infoRealisateurActeur("West side story").forEach(realActeur -> {
            log.info(realActeur.get("realisateur", String.class)+
                    " " +
                    realActeur.get("acteur", String.class)
            );
        });
        log.info("");*/
    }
}