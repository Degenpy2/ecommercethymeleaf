package popschool2020.cinema_thymeleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool2020.cinema_thymeleaf.CinemaThymeleafApplication;
import popschool2020.cinema_thymeleaf.dao.ClientRepository;
import popschool2020.cinema_thymeleaf.model.Client;

import java.util.Optional;

@Component
public class ClientTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(CinemaThymeleafApplication.class);

    @Autowired
    private ClientRepository repository;

    private void information(Client c){
        log.info(c.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        /*// fetch all customer

        log.info("Clients found with findAll();");
        log.info("----------------------");
        for(Client client : repository.findAll()) {
            log.info(client.toString());
        }
        log.info("");

        // fetch an individual customer by ID
        Optional<Client> client = repository.findById(3L);
        log.info("Customer found with findById(3L)");
        log.info("----------------------");
        if(client.isPresent()) {
            Client cli = client.get();
            log.info(client.get().toString());
            log.info("");
            repository.delete(cli);
            repository.findAll().forEach(this::information);
            log.info("");

        Optional<Client> optionalClient = repository.findById(1L);
        Client cl = optionalClient.get();
        cl.setAdresse("Tokyo");
        repository.save(cl);*/
    }
}
