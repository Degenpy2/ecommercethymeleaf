package popschool2020.cinema_thymeleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool2020.cinema_thymeleaf.CinemaThymeleafApplication;
import popschool2020.cinema_thymeleaf.dao.FilmRepository;
import popschool2020.cinema_thymeleaf.model.Film;

import java.sql.Date;
import java.time.LocalDateTime;

@Component
public class FilmTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(CinemaThymeleafApplication.class);

    @Autowired
    private FilmRepository repository;

    private void information(Film f) {
        log.info(f.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        /*
        log.info("Film found with findByGenre_Nature;");
        log.info("----------------------");
        repository.findByGenre_Nature("Guerre").forEach(movie -> {
            log.info(movie.toString());
        });
        log.info("");

        log.info("Film found with findByActeur_Nom;");
        log.info("----------------------");
        repository.findByActeur_Nom("Allen").forEach(movie -> {
            log.info(movie.toString());
        });
        log.info("");

        log.info("Film found with findByPays_nom;");
        log.info("----------------------");
        repository.findByPays_nom("USA").forEach(movie -> {
            log.info(movie.toString());
        });
        log.info("");

        log.info("Film found with findFilmsByGenre;");
        log.info("----------------------");
        repository.findFilmsByGenre().forEach(genre -> {
            log.info(genre.toString());
        });
        log.info("");

        log.info("Emprunt found with findByClient_Nclient;");
        log.info("----------------------");
        LocalDateTime date = LocalDateTime.now().minusDays(15L);
        Date sqlDate = Date.valueOf(date.toLocalDate());
        for(Film film: repository.findFilmEmprunteEnRetard(sqlDate)) {
            log.info(film.toString());
        }
        log.info("");*/
    }

}
