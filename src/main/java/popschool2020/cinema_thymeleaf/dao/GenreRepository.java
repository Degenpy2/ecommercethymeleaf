package popschool2020.cinema_thymeleaf.dao;

import org.springframework.data.repository.CrudRepository;
import popschool2020.cinema_thymeleaf.model.Genre;

import java.util.List;

public interface GenreRepository extends CrudRepository<Genre, Long> {
    List<Genre> findAll();
}
