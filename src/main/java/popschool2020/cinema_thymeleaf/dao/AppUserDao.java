package popschool2020.cinema_thymeleaf.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool2020.cinema_thymeleaf.model.AppUser;

import java.util.Optional;

public interface AppUserDao extends JpaRepository<AppUser,Long> {
    Optional<AppUser> findByName(String name);
}
