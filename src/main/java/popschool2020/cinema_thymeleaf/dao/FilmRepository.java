package popschool2020.cinema_thymeleaf.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool2020.cinema_thymeleaf.model.Film;
import popschool2020.cinema_thymeleaf.model.GenreDTO;

import javax.persistence.Tuple;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface FilmRepository extends CrudRepository<Film, Long> {
    List<Film> findAll();

    List<Film> findByGenre_Nature(String nature);

    List<Film> findByActeur_Nom(String nom);

    List<Film> findByPays_nom(String nomDuPays);

    Optional<Film> findByTitre(String titre);

    int countFilmByGenre_Nature(String nature);
    //Avec des DTO
    @Query("SELECT new popschool2020.cinema_thymeleaf.model.GenreDTO(f.genre.nature,count(f)) from Film f group by f.genre.nature")
    List<GenreDTO> findFilmsByGenre();

    @Query("select e.film from Emprunt e where e.retour='NON' and e.dateEmprunt < :dateRetour")
    List<Film> findFilmEmprunteEnRetard(Date dateRetour);



    // Avec des tuples, permet de ne pas avoir à créer un DTO
    @Query(value = "select f.realisateur as realisateur, f.acteur.nom as acteur from Film f where f.titre = :unTitre")
    List<Tuple> findRealisateurAndActeurByTitre(String unTitre);

}
