package popschool2020.cinema_thymeleaf.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import popschool2020.cinema_thymeleaf.model.Client;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client, Long> {
    List<Client> findAll();

    Optional<Client> findByNom(String nom);

    List<Client> findAllByOrderByNom();

    List<Client> findByPrenom(String prenom);

    List<Client> findByNomAndPrenom(String nom, String prenom);

    @Transactional
    int deleteByNom(String nom);
    /*
    Création, modification, suppression
    Finder, byNom, byPrenom, byNomAndPrenom
     */
}
