package popschool2020.cinema_thymeleaf.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import popschool2020.cinema_thymeleaf.model.Client;
import popschool2020.cinema_thymeleaf.model.Emprunt;
import popschool2020.cinema_thymeleaf.model.Film;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface EmpruntRepository extends CrudRepository<Emprunt, Long> {
    /*
Liste des films empruntables (ce jour)
Liste des films empruntés
Liste des films en retard

 */
    List<Emprunt> findAll();

    List<Emprunt> findByClient_Nclient(Long nclient);

    Optional<Emprunt> findByClientAndFilm(Client client, Film film);

    @Query("delete from Emprunt e where e.client.nom=:nom")
    @Modifying
    @Transactional
    int supprimerEmpruntsNomClient(String nom);

    @Query("delete from Emprunt e where e.client=:client")
    @Modifying
    @Transactional
    int supprimerEmpruntsClient(Client client);

    @Query("delete from Emprunt e where e.client.nclient=:numeroClient")
    @Modifying
    @Transactional
    int supprimerEmpruntsClientId(Long numeroClient);

    @Query("select f from Film f where not exists(select e from Emprunt e where e.film = f and e.retour ='NON') order by f.titre")
    List<Film> findFilmEmpruntable();

    @Query("select e.film from Emprunt e where e.retour='NON' and e.client = :client order by e.film.titre")
    List<Film> findFilmEmprunte(Client client);

    @Query("select e from Emprunt e where e.retour='NON'")
    List<Emprunt> findEmpruntNonRendu();

}
