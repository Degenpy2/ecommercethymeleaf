package popschool2020.cinema_thymeleaf.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool2020.cinema_thymeleaf.model.Acteur;

import javax.persistence.Tuple;
import java.sql.Date;
import java.util.List;

public interface ActeurRepository extends CrudRepository<Acteur, Long> {
    List<Acteur> findAll();

    List<Acteur> findByNom(String nom);

    List<Acteur> findByPrenom(String prenom);

    List<Acteur> findByNaissanceAfter(Date d);

    List<Acteur> findByNaissanceBefore(Date d);

    List<Acteur> findByNbrefilmsGreaterThan(int nbFilm);

    List<Acteur> findByNomContaining(String nom);

    List<Acteur> findByPays_nom(String nomDuPays);

    List<Acteur> findByPays_npays(Long npays);

    // Avec des tuples, permet de ne pas avoir à créer un DTO
    @Query(value = "select a.pays.nom as pays, count(a) as nombre from Acteur a GROUP BY a.pays.nom")
    List<Tuple> findActeurByPays();
    //



}